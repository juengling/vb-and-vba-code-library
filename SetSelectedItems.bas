'''
' Set selected items
'
' :param ListBox lst: Listbox
' :param Variant selections: Array of Strings to be selected
' :author: Christoph Jüngling, https://www.juengling-edv.de
'''
Public Sub SetSelected(ByRef lst As ListBox, ByVal selections As Variant)

Dim i As Integer
Dim j As Integer

'--------------------

For i = LBound(selections) To UBound(selections)
    For j = 0 To lst.ListCount - 1
        If lst.ItemData(j) = selections(i) Then lst.Selected(j) = True
    Next j
Next i

End Sub
