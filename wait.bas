Private Sub wait(ByVal seconds As Integer)

Const FULLDAY As Long = 86400 ' = 24 h * 60 min/h * 60 s/min

Dim t As Single
Dim d As Date

'------------------------

t = Timer() + seconds
d = Date

Do
    DoEvents
Loop Until Timer() + DateDiff("d", d, Date) * FULLDAY >= t

End Sub