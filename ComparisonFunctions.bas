'''
' Comparison functions
'
' Remove error handler if you use a global one
'
' :author: Christoph Juengling, `<https://www.juengling-edv.de>`_
'''

Option Explicit

Private Const CLASS_NAME = "ComparisonFunctions"

'''
' Compares two version numbers
'
' :param String v1:  First version number
' :param String v2:  Second version number
' :return:  -1: v1 < v2, 0: v1 = v2, +1: v1 > v2
' :rtype: Integer
' :author:  Christoph J�ngling, https://www.juengling-edv.de
'''
Public Function CompareVersions(ByVal v1 As String, ByVal v2 As String) As Integer

Const FUNCTION_NAME = "CompareVersions"

Dim i As Integer
Dim a1() As String
Dim a2() As String
Dim es As tSavedError

'-----------------------

On Error GoTo Catch

v1 = Trim(v1)
v2 = Trim(v2)

If v1 = "" And v2 = "" Then
    compareVersions = 0
ElseIf v1 = "" And v2 <> "" Then
    compareVersions = -1
ElseIf v1 <> "" And v2 = "" Then
    compareVersions = 1
Else
    ' Prepare arrays
    
    a1 = Split(v1, ".")
    If UBound(a1) < 3 Then ReDim Preserve a1(0 To 3)
    For i = 0 To 3
        If a1(i) = "" Then a1(i) = "0"
    Next i
    For i = LBound(a1) To UBound(a1)
        If Not IsNumeric(a1(i)) Then Err.Raise 13, , "Malformed version number: " & v1
    Next i
    
    a2 = Split(v2, ".")
    If UBound(a2) < 3 Then ReDim Preserve a2(0 To 3)
    For i = 0 To 3
        If a2(i) = "" Then a2(i) = "0"
    Next i
    For i = LBound(a2) To UBound(a2)
        If Not IsNumeric(a2(i)) Then Err.Raise 13, , "Malformed version number: " & v2
    Next i
    
    ' Compare version arrays
    compareVersions = 0
    For i = 0 To 3
        If CInt(a1(i)) < CInt(a2(i)) Then
            compareVersions = -1
            Exit For
        ElseIf CInt(a1(i)) > CInt(a2(i)) Then
            compareVersions = 1
            Exit For
        End If
    Next i
End If

'-----------------------
Final:
On Error GoTo 0
RaiseSavedError es

Exit Function

'-----------------------
Catch:
es = SaveError(CLASS_NAME, FUNCTION_NAME, Erl)
Resume Final
Resume

End Function
