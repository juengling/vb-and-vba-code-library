''
' Find and replace strings as special placerholders
'
' @param    original        Original string
' @param    SearchReplace() Pairs of search and replace strings
' @return   Original with replacements
'
Public Function StrReplace(ByVal original As String, ParamArray SearchReplace()) As String

Dim strResult As String
Dim i As Integer

'------------------------

strResult = original

For i = LBound(SearchReplace) To UBound(SearchReplace) - 1 Step 2
    strResult = Replace(strResult, "{" & SearchReplace(i) & "}", SearchReplace(i + 1))
Next i

StrReplace = strResult

End Function
