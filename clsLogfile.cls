VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsLogfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
' Provide functionality to write a log file
'
' @author   Christoph Juengling
'
Option Explicit

Private Const delimiter = ";"

Public append As Boolean
Private m_strLoggedText As String
Private m_strFilename As String

Private fso As Scripting.FileSystemObject
Private file As Scripting.file

Private Sub Class_Initialize()

append = True
filename = ""

Set fso = New Scripting.FileSystemObject

End Sub

Private Sub Class_Terminate()

Set file = Nothing
Set fso = Nothing

End Sub

''
' Write a line of text into the log file
'
' @param    text   Text
' @author   Christoph Juengling
'
Public Sub WriteLine(Optional ByVal text As String = "")

Const FUNCTION_NAME = "WriteLine"

Dim app As Integer
Dim ts As Scripting.TextStream
Dim es As tSavedError

'------------------------

On Error GoTo Catch

If filename = "" Then RaiseProjectError ERR_FILEACCESS_ERROR, , , "No log file specified"

If append Then app = ForAppending Else app = ForWriting
Set ts = file.OpenAsTextStream(app)
ts.WriteLine GetWinUserName() & "@" & GetWinComputerName() & delimiter & text

If isInIDE() Then Debug.Print Replace(text, delimiter, vbTab)

m_strLoggedText = AddToList(m_strLoggedText, text, vbNewLine)

'------------------------
Final:
On Error Resume Next
ts.Close
Set ts = Nothing

On Error GoTo 0
RaiseSavedError es
Exit Sub

'------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Select Case es.number
    Case Else
        Debug.Print es.number, es.source, es.description
End Select
Resume Final
Resume ' for test purposes only

End Sub

Public Property Get filename() As String

filename = m_strFilename

End Property

Public Property Let filename(ByVal strFilename As String)

Const FUNCTION_NAME = "filename"

Dim es As tSavedError

'------------------------

On Error GoTo Catch

If StrComp(m_strFilename, strFilename, vbTextCompare) <> 0 Then
    If Not file Is Nothing Then Set file = Nothing
    If Not fso.FileExists(strFilename) Then fso.CreateTextFile (strFilename)
    Set file = fso.GetFile(strFilename)
End If

m_strFilename = strFilename
m_strLoggedText = ""

'------------------------
Final:
On Error GoTo 0
RaiseSavedError es
Exit Property

'------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Select Case es.number
    Case Else
        Debug.Print es.number, es.source, es.description
End Select
Resume Final
Resume ' for test purposes only

End Property

Public Property Get loggedText() As String

loggedText = m_strLoggedText

End Property


''
' Write function protocol
'
' @param    classname       Name of the class or module
' @param    functionname    Name of the function
' @param    text            Text to be written
' @param    args            Arguments (name/value pairs)
' @author   Christoph Juengling
'
Public Sub Protocol(ByVal classname As String, ByVal functionname As String, ByVal text As String, ParamArray args() As Variant)

Const FUNCTION_NAME = "Protocol"

Dim caller As String
Dim output As String
Dim i As Integer
Dim argname As String
Dim argvalue As String
Dim es As tSavedError

'------------------------

On Error GoTo Catch

' Usual text
output = format(Now, "yyyy-mm-dd hh:nn:ss")
output = AddToList(output, classname, delimiter)
output = AddToList(output, functionname, ".")
output = AddToList(output, caller, delimiter)
output = AddToList(output, text, delimiter)
WriteLine output

' Arguments
output = ""
For i = LBound(args) To UBound(args) Step 2
    argname = args(i)
    argvalue = args(i + 1)
    output = AddToList(argname, argvalue, " = ")
    WriteLine delimiter & delimiter & output
Next i

'------------------------
Final:
On Error Resume Next
' ... Aufräumarbeiten ...

On Error GoTo 0
RaiseSavedError es
Exit Sub

'------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Select Case es.number
    Case 9 ' Subscript out of range
        ClearError es
        Resume Next
        
    Case Else
        Debug.Print es.number, es.source, es.description
End Select
Resume Final
Resume ' for test purposes only

End Sub
