''
' Function to attach tables to the current Access database
'
' @author  Christoph Juengling <chris@juengling-edv.de>
' @comment Needs clsAttachTables to work
'
Public Function AttachTables()

Const FUNCTION_NAME = "AttachTables"

Dim at As clsAttachTables
Dim es As tSavedError

'-----------------------

On Error GoTo Catch

Set at = New clsAttachTables
at.backendPath = BuildPath(ReadFromIni("Options", "BackendPath"), ReadFromIni("Options", "BackendFile"))
at.attach

'-----------------------
Final:
On Error Resume Next
Set at = Nothing

On Error GoTo 0
RaiseSavedError es

Exit Function

'-----------------------
Catch:
es = SaveError(CLASS_NAME, FUNCTION_NAME, Erl)

Select Case es.Number
    Case Else
        Debug.Print es.Number, es.source, es.Description
End Select

Resume Final
Resume

End Function