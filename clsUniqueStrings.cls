VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsUniqueStrings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
' Ensure strings to be unique
'
' @throws   ERR_DUPLICATE_NAME
' @author   Christoph Juengling
'
Option Explicit

' Public properties without processing
Public FunctionName As String
Public Entity As String
Public CompareMethod As VbCompareMethod

' Events
Public Event NameIsNotUnique(FunctionName As String, Entity As String, name As String)

Private Type tValue
    FunctionName As String
    Entity As String
    Content As String
    index As Long
End Type

Private Values() As tValue
Private m_bolIsUnique As Boolean
Private lngindexStart As Long
''
' Add a new string to the collection and check uniqueness
'
' @param    Value   New value
' @param    index   index for new value (not validated to be unique!)
' @return   index number
'
Public Function add(value As String, Optional ByVal index As Long = -1) As Long

Const FUNCTION_NAME = "Add"

Dim i As Long
Dim lLastFoundindex As Long
Dim es As tSavedError

'------------------------

On Error GoTo Catch

lLastFoundindex = 0
m_bolIsUnique = True
If value = "" Then GoTo Final

' Check if new string is already in array
For i = 0 To UBound(Values)
    With Values(i)
        If .Content = "" Then Exit For
        If StrComp(.FunctionName, FunctionName, vbTextCompare) = 0 And StrComp(.Entity, Entity, vbTextCompare) = 0 Then
            lLastFoundindex = .index

            If StrComp(.Content, value, CompareMethod) = 0 Then
                m_bolIsUnique = False
                add = .index
                RaiseEvent NameIsNotUnique(FunctionName, Entity, value)
                GoTo Final
            End If
        End If
    End With
Next i

' Increase array if necessary
If i > UBound(Values) Then ReDim Preserve Values(0 To UBound(Values) + 10)

' Add new string to array
With Values(i)
    .FunctionName = FunctionName
    .Entity = Entity
    .Content = value
    If index = -1 And lLastFoundindex <> 0 Then
        .index = lLastFoundindex + 1
    Else
        .index = index
    End If
    add = .index
End With

' Initialize new part of array
ClearFrom i + 1

'------------------------
Final:
On Error GoTo 0
RaiseSavedError es
Exit Function

'------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Select Case es.number
    Case Else
        Debug.Print es.number, es.source, es.description
End Select
add = -1
Resume Final
Resume    ' for test purposes only

End Function

Private Sub Class_Initialize()

Clear
CompareMethod = vbTextCompare
FunctionName = ""
Entity = ""

End Sub

Public Property Get IsUnique() As Boolean

IsUnique = m_bolIsUnique

End Property

Public Sub Clear(Optional ByVal indexStart As Long = 1)

lngindexStart = indexStart
ClearFrom -1

End Sub

''
' Dump array contents to debug window
'
' @remarks  Use for testing purposes only
'
Public Sub Dump(Optional filename As String = "")

Const FUNCTION_NAME = "Dump"

Dim i As Integer
Dim f As Integer
Dim s As String
Dim es As tSavedError

'------------------------

On Error GoTo Catch

s = format(Now, "General Date")
s = String(10, "*") & " " & s & " " & String(68 - Len(s), "*") & vbNewLine

For i = LBound(Values) To UBound(Values)
    With Values(i)
        If .Content <> "" Then
            s = s & .FunctionName & vbTab & .Entity & vbTab
            s = s & .Content
            s = s & vbNewLine
        End If
    End With
Next i

s = s & String(80, "*") & vbNewLine

If filename <> "" Then
    f = FreeFile()
    Open filename For Append Access Write Lock Write As f
    Print #f, s
Else
    Debug.Print s
End If

'------------------------
Final:
On Error Resume Next
Close f

On Error GoTo 0
RaiseSavedError es
Exit Sub

'------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Select Case es.number
    Case Else
        Debug.Print es.number, es.source, es.description
End Select
Resume Final
Resume ' for test purposes only

End Sub

''
' Clear array from given index
'
' @param    intStart   Start index, -1 = complete reinitialise
'
Private Sub ClearFrom(intStart As Integer)

Dim i As Integer

'------------------------

If intStart < 0 Then
    ReDim Values(0)
    intStart = 0
End If

For i = intStart To UBound(Values)
    With Values(i)
        .FunctionName = ""
        .Entity = ""
        .Content = ""
        .index = -1
    End With
Next i

End Sub



''
' Returns number of elements with the given FunctionName and/or Entity
'
' @param    FunctionName   Function Name
' @param    Entity         Entity
' @return   Number of elements
'
Public Function count(Optional ByVal FunctionName As String = "", Optional ByVal Entity As String = "") As Long

Dim i As Long
Dim lngResult As Long

'------------------------

lngResult = 0

For i = 0 To UBound(Values)
    With Values(i)
        If .Content = "" Then Exit For
        If StrComp(.FunctionName, FunctionName, vbTextCompare) = 0 And StrComp(.Entity, Entity, vbTextCompare) = 0 Then
            lngResult = lngResult + 1
        End If
    End With
Next i

count = lngResult

End Function
