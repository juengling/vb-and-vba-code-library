Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
    ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, _
    ByVal lpDefault As String, _
    ByVal lpReturnedString As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String _
) As Long


'=======================================================================================
' Purpose : Read setting from INI file
' Params  : Section = Section name
'           Key = Key name
'           [Default] = Default value if no entry was found
'           [IniFile] = Specific INI file
' Result  : INI setting or default value
' Author  : Unknown
'=======================================================================================

Public Function ReadFromIni(ByVal section As String, ByVal key As String, Optional ByVal default As String = "", Optional ByVal IniFile As String = "") As String

Const BUFFER_LENGTH As Long = 1024

Dim strTemp As String * BUFFER_LENGTH
Dim lngLength As Long
Dim fst As clsFileSystemTools
'---------------------------

Set fst = New clsFileSystemTools

fst.path = CurrentProject.path
fst.nameext = CurrentProject.name
fst.ext = "ini"
If IniFile = "" Then IniFile = fst.fullpath
strTemp = Space(BUFFER_LENGTH)
lngLength = GetPrivateProfileString(section, key, default, strTemp, BUFFER_LENGTH, IniFile)
ReadFromIni = Left$(strTemp, lngLength)

Set fst = Nothing

End Function

