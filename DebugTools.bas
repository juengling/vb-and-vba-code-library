''
' Debug Tools
'
' @author   Christoph Juengling
'
Option Explicit
' Option Compare Database ' Access only

''
' Display name and caption of open forms in debug window
'
' @author   Christoph Juengling
'
Public Sub f()

Dim i As Integer

Debug.Print Forms.Count & " " & IIf(Forms.Count = 1, "form", "forms") & " open at " & Now
For i = 0 To Forms.Count - 1
    Debug.Print i; Tab(5); Forms(i).Name; Tab(30); Forms(i).Caption
Next i

End Sub
