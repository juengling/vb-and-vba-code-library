'''
' Ersatzfunktion f�r MsgBox
'
' :param String Prompt:          Der Haupt-Text der Message Box
' :param VbMsgBoxStyle Buttons:  Label und Buttons
' :param String Title:           Titel
' :param String HelpFile:        Helpfile
' :param Long HelpContext:       Help Context
' :param String Prompt2:         Zweiter Absatz
' :param String Prompt3:         Dritter Absatz
' :return: Gedr�ckter Button
' :rtype: Long
' :author: Christoph J�ngling, Thomas M�ller
'''
Public Function MsgBox( _
    Prompt As String, _
    Optional Buttons As VbMsgBoxStyle = vbOKOnly + vbInformation, _
    Optional Title As String = "", _
    Optional HelpFile As String = "Message", _
    Optional HelpContext As Long = 0, _
    Optional Prompt2 As String = "", _
    Optional Prompt3 As String = "") As Long

Dim result As Long

WizHook.key = 51488399
result = WizHook.WizMsgBox(Prompt & "@" & Prompt2 & "@" & Prompt3 & "@", Title, Buttons, HelpContext, HelpFile)

MsgBox = result

End Function
