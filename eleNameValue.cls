VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "eleNameValue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
' Just a container for the elements of a key-value store
'
' @author   Christoph Juengling

Option Explicit

Public name As String
Public value As Variant
Attribute value.VB_VarUserMemId = 0

Private Sub Class_Initialize()

name = ""
value = Null

End Sub



