''
' Class to collect file names/paths from file system
'
' @remarks  Uses: Microsoft Scripting Runtime
' @author   Christoph Juengling <christoph@juengling-edv.de>
' @link   https://bitbucket.org/juengling/vb-and-vba-code-library
'
Option Explicit

' Make use of FileSystemObject to ease operations
Private fso As Scripting.FileSystemObject ' Needs "Microsoft Scripting Runtime" reference

''
' Array to hold the file collection
Private FileList() As String

Private fileListIndex As Long
Private bReady As Boolean

' Public properties
Private m_strStartPath As String
Private m_strExtension As String
Private m_bRecursive As Boolean
Private m_bNoPath As Boolean
Private m_bNoExtension As Boolean
Private m_bolCollFiles As Boolean
Private m_bolCollFolders As Boolean


''
' Determines whether the file extension should be reported or not
'
' @return   Boolean value
'
Public Property Get NoExtension() As Boolean

NoExtension = m_bNoExtension

End Property

''
' Determines whether the file extension should be reported or not
'
' @param bValue New value for extension reporting status
'
Public Property Let NoExtension(ByVal bValue As Boolean)

m_bNoExtension = bValue

End Property

''
' Determines whether the file path should be reported or not
'
' @return   Boolean value
'
Public Property Get NoPath() As Boolean

NoPath = m_bNoPath

End Property

''
' Determines whether the file path should be reported or not
'
' @param bValue New value for path reporting status
'
Public Property Let NoPath(ByVal bValue As Boolean)

m_bNoPath = bValue
bReady = False

End Property

''
' Determines whether the file collection should be recurse into subfolders or not
'
' @return   Boolean value
'
Public Property Get Recursive() As Boolean

Recursive = m_bRecursive

End Property

''
' Determines whether the file collection should be recurse into subfolders or not
'
' @param bValue New value for recurse status
'
Public Property Let Recursive(ByVal bValue As Boolean)

m_bRecursive = bValue
bReady = False

End Property


''
' Gets the file name extension for the lookup functionality
'
' @return   Extension
'
Public Property Get Extension() As String

Extension = m_strExtension

End Property

''
' Sets the file name extension for the lookup functionality
'
' @param strValue New value for file extension
'
Public Property Let Extension(ByVal strValue As String)

strValue = Trim(strValue)
Do While Left(strValue, 1) = "." Or Left(strValue, 1) = "*"
    strValue = Mid(strValue, 2)
Loop

m_strExtension = LCase(strValue)
bReady = False

End Property


''
' Starting path of the collection
'
' @return   Starting path
'
Public Property Get StartPath() As String

StartPath = m_strStartPath

End Property

''
' Starting path of the collection
'
' @param strValue New value for starting path
'
Public Property Let StartPath(ByVal strValue As String)

m_strStartPath = strValue
bReady = False

End Property

Private Sub Class_Initialize()

Set fso = New Scripting.FileSystemObject

m_strStartPath = ""
m_strExtension = ""
m_bRecursive = False
m_bNoPath = False
m_bNoExtension = False
m_bolCollFiles = True
m_bolCollFolders = False
bReady = False

fileListIndex = 0

End Sub

Private Sub Class_Terminate()

Set fso = Nothing

End Sub


''
' Starts the file collecting
'
Private Sub CollectFiles()

Const FUNCTION_NAME = "CollectFiles"

Dim StartFolder As Scripting.Folder

'-----------------

On Error GoTo Catch

If m_strStartPath <> "" And Not bReady Then
    ' Clear array and reset index
    ReDim FileList(0 To 9)
    fileListIndex = -1
    
    Set StartFolder = fso.GetFolder(m_strStartPath)
    ProcessFolder StartFolder
    
    ' Clear empty entries at the end
    If fileListIndex >= 0 Then ReDim Preserve FileList(0 To fileListIndex)
    
    bReady = True
End If

Exit Sub

'-----------------

Catch:
Select Case err.Number
    Case 76  ' Path not found
        Err.Raise Err.Number, Err.source & " / " & TypeName(Me) & "." & FUNCTION_NAME, "Start folder """ & m_strStartPath & """ not found!", Err.Helpfile, Err.HelpContext

    Case Else
        Err.Raise Err.Number, Err.source & " / " & TypeName(Me) & "." & FUNCTION_NAME, Err.Description, Err.Helpfile, Err.HelpContext
End Select

End Sub


''
' Returns the file collection
'
' @return   Array with the found file names
'
Public Function GetFileList() As String()

CollectFiles
SortFileList
GetFileList = FileList()

End Function


''
' Processing of a folder to collect files
'
' @param    fld   Folder object
' @remarks Recurses to dive into the folder structure
'
Private Sub ProcessFolder(fld As Scripting.folder)

Dim Files As Scripting.Files
Dim File As Scripting.File
Dim Folders As Scripting.Folders
Dim Folder As Scripting.Folder

'-----------------

If m_bRecursive Or m_bolCollFolders Then
    Set Folders = fld.SubFolders
    For Each Folder In Folders
        If m_bolCollFolders Then add Folder.name, ""
        If m_bRecursive Then ProcessFolder Folder
    Next Folder
End If

If m_bolCollFiles Then
    Set Files = fld.Files
    For Each File In Files
        If LCase(Right(File.name, Len(m_strExtension) + 1)) = "." & m_strExtension Then
            add File.path, File.name
        End If
    Next File
End If

End Sub

''
' Add path/filename to the colllection
'
' @param    Path        Path
' @param    FileName    FileName
'
Private Sub add(path As String, filename As String)

Dim s As String

'------------------------

' increase size of array, if necessary
If fileListIndex >= UBound(FileList) Then ReDim Preserve FileList(0 To fileListIndex + 10)

fileListIndex = fileListIndex + 1

If filename <> "" Then
    If m_bNoExtension Then
        s = fso.GetBaseName(filename)
    Else
        s = filename
    End If
End If

If Not m_bNoPath Then s = fso.BuildPath(path, s)

FileList(fileListIndex) = s

End Sub

Public Property Get CollFiles() As Boolean

CollFiles = m_bolCollFiles

End Property

Public Property Let CollFiles(ByVal bolCollFiles As Boolean)

m_bolCollFiles = bolCollFiles
If Not bolCollFiles Then m_bolCollFolders = True

End Property

Public Property Get CollFolders() As Boolean

CollFolders = m_bolCollFolders

End Property

Public Property Let CollFolders(ByVal bolCollFolders As Boolean)

m_bolCollFolders = bolCollFolders
If Not bolCollFolders Then m_bolCollFiles = True

End Property


''
' Sort the collected file list
'
' @remarks  A simple bubble sort will do the trick, because this won't be too many files ...
' @author   Christoph Juengling
'
Private Sub SortFileList()

Dim s As String
Dim i As Integer
Dim j As Integer
Dim bAgain As Boolean

'------------------------

Do
    bAgain = False
    For i = LBound(FileList) To UBound(FileList) - 1
        For j = i + 1 To UBound(FileList)
            If StrComp(FileList(i), FileList(j), vbTextCompare) = 1 Then
                ' Switch entries
                s = FileList(i)
                FileList(i) = FileList(j)
                FileList(j) = s
                bAgain = True
            End If
        Next j
    Next i
Loop While bAgain

End Sub
