''
' Singleton patterns
'
' @remarks This is just a template. To do: Inspect and adapt :-)
' @see http://www.oodesign.com/singleton-pattern.html
'
Option Explicit

Private m_ObjectName As clsClassName

Public Property Get ObjectName() As clsClassName
    
    If m_ObjectName Is Nothing Then Set m_ObjectName = New clsClassName
    Set ObjectName = m_ObjectName
    
End Property
