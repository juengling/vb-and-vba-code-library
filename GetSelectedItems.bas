'''
' Return selected items
'
' :param ListBox lst: Listbox
' :param String delimiter: Delimiter between elements
' :param Boolean sql: Special additions for SQL
' :return: Selected items for direct use in a SQL WHERE condition
' :rtype: String
' :author: Christoph Jüngling, https://www.juengling-edv.de
'''
Public Function GetSelected(ByRef lst As ListBox, Optional ByVal delimiter As String = ",", Optional ByVal sql As Boolean = True) As String

Dim x As Variant
Dim i As Long
Dim result As String

'----------------

result = ""
For i = 0 To lst.ListCount - 1
    If lst.Selected(i) Then result = add2list(result, IIf(sql, "'", "") & lst.Column(0, i) & IIf(sql, "'", ""), delimiter)
Next i

GetSelected = result

End Function
