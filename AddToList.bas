Public Function AddToList(ByVal orig As String, ByVal NewString As String, ByVal delimiter As String) As String

Dim result As String

result = orig

If NewString <> "" Then
    If result <> "" Then result = result & delimiter
    result = result & NewString
End If

AddToList = result

End Function