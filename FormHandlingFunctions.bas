Attribute VB_Name = "FormHandlingFunctions"
''
' Several form handling functions
'
' @author   Christoph Juengling <christoph@juengling-edv.de>
' @link   https://bitbucket.org/juengling/vb-and-vba-code-library
'
Option Explicit

Private Const CLASS_NAME = "FormHandlingFunctions"

''
' Check if form is loaded or not without touching its standard object variable
'
' @param    FormName   Name of the form to be checked
' @return   -1: form is not loaded, >=0: Index in Forms() selection
'
Public Function IsLoaded(FormName As String) As Integer

Dim i As Integer

'-------------------

IsLoaded = -1
For i = 0 To Forms.Count - 1
    If StrComp(Forms(i).Name, FormName, vbTextCompare) = 0 Then
        IsLoaded = i
        Exit For
    End If
Next i

End Function
