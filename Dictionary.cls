VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Dictionary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
' Class module to implement Python's Dictionary data type
'
' @remarks  See https://msdn.microsoft.com/en-us/library/aa262338%28v=vs.60%29.aspx
' @author   Christoph Juengling

Option Explicit

Private mycoll As Collection

Private Sub Class_Initialize()

Set mycoll = New Collection

End Sub

Private Sub Class_Terminate()

Set mycoll = Nothing

End Sub

Public Function add(ByVal key As String, ByVal value As Variant) As eleNameValue

Dim element As eleNameValue

'------------------------

' Invalid argument
If Trim(key) = "" Then err.Raise 610, typeName(Me) & ".add", "Key must not be an empty string!"

On Error Resume Next
Set element = mycoll.item(key)

On Error GoTo 0
If element Is Nothing Then
    Set element = New eleNameValue
    With element
        .name = key
        .value = value
    End With

    mycoll.add element, key
Else
    element.value = value
End If

Set add = element

End Function

Public Function count() As Long

count = mycoll.count
    
End Function

Public Sub delete(ByVal key As String)

mycoll.Remove key

End Sub

Public Function item(ByVal key As String) As eleNameValue
Attribute item.VB_UserMemId = 0

On Error Resume Next ' Return "Nothing" if no element found
Set item = mycoll.item(key)

End Function

Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

Set NewEnum = mycoll.[_NewEnum]

End Function
