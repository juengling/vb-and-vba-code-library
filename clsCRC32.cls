VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCRC32"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
' Compute CRC-32 from given input
'
' @remarks  Code token from http://www.freevbcode.com/ShowCode.asp?ID=655
' @remarks  AI, AO, DI, DO, Serials
' @remarks  Name, Modul address, MinRaw, MaxRaw
' @author   Christoph Juengling
'
Option Explicit
Option Compare Text

Private Crc32Table(255) As Long
Private crc32 As Long

Private Sub Class_Initialize()

reset

End Sub

''
' Reset internal data structures
'
' @author   Christoph Juengling
'
Public Sub reset()

crc32 = InitCrc32()

End Sub

''
' Add a new string
'
' @param    item    New string
' @author   Christoph Juengling
'
Public Sub add(item As Variant)

crc32 = AddCrc32(item, crc32)

End Sub

''
' Init CRC-32
'
' @param    Seed
' @param    Precondition
' @return   Return value
'
Private Function InitCrc32( _
    Optional ByVal Seed As Long = &HEDB88320, _
    Optional ByVal Precondition As Long = &HFFFFFFFF _
) As Long

Dim iBytes As Integer
Dim iBits As Integer
Dim lCrc32 As Long
Dim lTempCrc32 As Long

'------------------------

On Error Resume Next

' Iterate 256 times
For iBytes = 0 To 255
    ' Initiate lCrc32 to counter variable
    lCrc32 = iBytes

    ' Now iterate through each bit in counter byte
    For iBits = 0 To 7
        ' Right shift unsigned long 1 bit
        lTempCrc32 = lCrc32 And &HFFFFFFFE
        lTempCrc32 = lTempCrc32 \ &H2
        lTempCrc32 = lTempCrc32 And &H7FFFFFFF

        ' Now check if temporary is less than zero and then mix Crc32 checksum with Seed value
        If (lCrc32 And &H1) <> 0 Then
            lCrc32 = lTempCrc32 Xor Seed
        Else
            lCrc32 = lTempCrc32
        End If
    Next

    ' Put Crc32 checksum value in the holding array
    Crc32Table(iBytes) = lCrc32
Next

' After this is done, set function value to the precondition value
InitCrc32 = Precondition

End Function

''
' Compute CRC-32
'
' @param    Item    New item
' @param    Crc32   Old checksum
' @return   New checksum
'
Private Function AddCrc32(ByVal item As String, ByVal crc32 As Long) As Long

Dim bCharValue As Byte
Dim iCounter As Integer
Dim lindex As Long
Dim lAccValue As Long
Dim lTableValue As Long

'------------------------

On Error Resume Next

' Iterate through the string that is to be checksum-computed
For iCounter = 1 To Len(item)
    ' Get ASCII value for the current character
    bCharValue = Asc(Mid$(item, iCounter, 1))

    ' Right shift an Unsigned Long 8 bits
    lAccValue = crc32 And &HFFFFFF00
    lAccValue = lAccValue \ &H100
    lAccValue = lAccValue And &HFFFFFF

    ' Now select the right adding value from the holding table
    lindex = crc32 And &HFF
    lindex = lindex Xor bCharValue
    lTableValue = Crc32Table(lindex)

    ' Then mix new Crc32 value with previous accumulated Crc32 value
    crc32 = lAccValue Xor lTableValue
Next

AddCrc32 = crc32

End Function

''
' Return CRC-32
'
' @return   CRC-32
'
Public Property Get result() As String

result = Hex(crc32 Xor &HFFFFFFFF)

End Property
