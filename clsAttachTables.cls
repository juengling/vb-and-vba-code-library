Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'=======================================================================================
' Module  : clsAttachTables (Klassenmodul)
' Purpose : Manage actions for re-attaching linked tables
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
' Throws  : Database errors
' Raises  : Starting, NextTable, Aborted, Finished
' Uses    : (ref) Microsoft ADO Ext. 2.8 for DDL and Security (ADOX)
'           (ref) Microsoft ActiveX Data Objects 2.8 Library (ADODB)
'           (mod) ErrorDefinitions
'=======================================================================================

Option Compare Database
Option Explicit

Private Const TABLE_NAME = "EingebundeneTabellen"

Private cat As ADOX.Catalog

' Public events
Public Event Starting(NumberOfTables As Long)
Public Event NextTable(TableName As String)
Public Event TableDone(TableName As String)
Public Event Aborted(Message As String)
Public Event Finished(backendPath As String)

' Public properties without any checks
Public backendPath As String
Public checkOnly As Boolean

'=======================================================================================
' Purpose : (Re-)attach planned tables
' Throws  : ERR_ATTACHING_TABLES, all other errors
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
'=======================================================================================

Public Sub attach()

Const FUNCTION_NAME = "attach"

Dim rs As ADODB.Recordset
Dim tbl As ADOX.Table
Dim TableName As String
Dim TableType As String

'----------------------------

If Trim(backendPath) = "" Then Err.Raise GetVBErrorNumber(ERR_ATTACHING_TABLES), TypeName(Me) & "." & FUNCTION_NAME, "Missing Backend Path!"

RaiseEvent Starting(0)

' Open Control table
Set rs = New ADODB.Recordset
With rs
    .ActiveConnection = CurrentProject.Connection
    .CursorLocation = adUseClient
    .CursorType = adOpenDynamic
    .LockType = adLockPessimistic
    .Source = "SELECT Tabellenname, backendTableName FROM " & TABLE_NAME & " ORDER BY zuerst, Tabellenname"
    .Open
    RaiseEvent Starting(.RecordCount)
End With

' Run through the planned tables
Do Until rs.EOF
    ' Check if a table with this name already exists
    TableName = CStr(rs!Tabellenname)
    RaiseEvent NextTable(TableName)
    TableType = cat.Tables(TableName).Type
    
    Select Case TableType
        Case "TABLE"
            Err.Raise GetVBErrorNumber(ERR_ATTACHING_TABLES), "", "Attached table """ & TableName & """ is stored locally."
            
        Case "LINK"
            ' Check current link
            If cat.Tables(TableName).Properties("Jet OLEDB:Link Datasource") <> backendPath Then
                Debug.Print "L�sche " & TableName
                DoCmd.DeleteObject acTable, CStr(TableName)
                attachNew TableName, Nz(rs!backendTableName)
            End If
            
        Case "NOTFOUND" ' This is not a real type, see error handling
            attachNew TableName, Nz(rs!backendTableName)
            
        Case Else
            Err.Raise GetVBErrorNumber(ERR_ATTACHING_TABLES), "", "Table """ & TableName & """ is of unknown type!"
    End Select
    
    RaiseEvent TableDone(TableName)
    rs.MoveNext
Loop

rs.Close
RaiseEvent Finished(backendPath)

'----------------------------
Final:
Set rs = Nothing

Set cat = Nothing
Exit Sub

'----------------------------
' Error routines for usage of vbWatchDog

ErrEx.Catch 3265, -2147217857
    TableType = "NOTFOUND"
    Resume Next

ErrEx.Catch 3125
    Debug.Print Err.Description
    TableType = "NOTFOUND"
    Resume Next

End Sub



Private Sub Class_Initialize()

backendPath = ""
checkOnly = False

' Catalog of current database
Set cat = New ADOX.Catalog
cat.ActiveConnection = CurrentProject.Connection

End Sub



'=======================================================================================
' Purpose : Attach new table
' Params  : TableName = name of the table in this database (frontend)
'           BackendTableName = name of this table in backend database
' Throws  : all errors
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
'=======================================================================================

Private Sub attachNew(TableName As String, Optional backendTableName As String = "")

Const FUNCTION_NAME = "attachNew"

Dim tbl As ADOX.Table

'---------------------------

' Create the new Table in the current catalog
Set tbl = New ADOX.Table
With tbl
    .Name = TableName
    Set .ParentCatalog = cat

    ' Set the properties to create the link
    .Properties("Jet OLEDB:Link Provider String") = "MS Access"
    .Properties("Jet OLEDB:Link Datasource") = backendPath
    .Properties("Jet OLEDB:Remote Table Name") = IIf(backendTableName <> "", backendTableName, TableName)
    .Properties("Jet OLEDB:Create Link") = True
End With

' Append the table to the Tables collection
cat.Tables.Append tbl

End Sub

Private Sub Class_Terminate()

Set cat = Nothing

End Sub



'=======================================================================================
' Purpose : Drop all linked tables
' Throws  : All encountered errors
' Author  : Christoph J�ngling <Christoph@Juengling-EDV.de>
' Remarks : Dropping all tables decreases the size of the MDB file.
'           After compacting it, this is more convenient for building a setup.
'=======================================================================================

Public Sub drop()

Const FUNCTION_NAME = "drop"

Dim tbl As ADOX.Table
Dim found As Boolean

'-----------------------

Do
    found = False
    For Each tbl In cat.Tables
        If StrComp(tbl.Type, "LINK", vbTextCompare) = 0 Then
            cat.Tables.Delete tbl.Name
            found = True
        End If
    Next tbl
Loop While found

End Sub