VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFileSystemTools"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
' File System Tools
'
' @author   Christoph Juengling
'
Option Explicit

Private m_Fullpath As String
Private m_Drive As String
Private m_Path As String
Private m_Name As String
Private m_Extension As String
Private m_NameExt As String

Private fso As Scripting.FileSystemObject

Private Sub Class_Initialize()

m_Fullpath = ""
reset

End Sub

Private Sub Class_Terminate()

Set fso = Nothing

End Sub

Private Sub reset()

m_Drive = ""
m_Path = "\"
m_Name = ""
m_Extension = ""
m_NameExt = ""

End Sub
Public Property Get fullPath() As String

fullPath = m_Fullpath

End Property

Public Property Let fullPath(ByVal strfullpath As String)

#If DEBUG_MODE > 2 Then
    Log.Protocol typeName(Me), "fullPath", "", "strfullpath", strfullpath
#End If

m_Fullpath = strfullpath
SplitPath

End Property

Public Property Get drive() As String

drive = m_Drive

End Property

Public Property Let drive(ByVal strdrive As String)

#If DEBUG_MODE > 2 Then
    Log.Protocol typeName(Me), "drive", "strdrive", strdrive
#End If

If Len(strdrive) > 0 Then
    m_Drive = Left(strdrive, 1)
Else
    m_Drive = ""
End If

BuildPath

End Property

Public Property Get path() As String

If m_Drive <> "" Then
    path = m_Drive & ":" & m_Path
Else
    path = m_Path
End If

End Property

Public Property Let path(ByVal strPath As String)

Dim c As Integer

'------------------------

#If DEBUG_MODE > 2 Then
    Log.Protocol typeName(Me), "path", "", "strPath", strPath
#End If

c = InStr(1, strPath, ":")
If c = 0 Then
    Me.pathonly = strPath
Else
    Me.drive = Left(strPath, c - 1)
    Me.pathonly = Mid(strPath, c + 1)
End If

End Property

Public Property Get pathonly() As String

pathonly = m_Path

End Property

Public Property Let pathonly(ByVal strPath As String)

#If DEBUG_MODE > 2 Then
    Log.Protocol typeName(Me), "pathonly", "", "strPath", strPath
#End If

m_Path = strPath

If Len(m_Path) = 0 Then
    m_Path = "\"
Else
    If Left(m_Path, 1) <> "\" Then m_Path = "\" & m_Path
    If Right(m_Path, 1) <> "\" Then m_Path = m_Path & "\"
End If

BuildPath

End Property

Public Property Get name() As String

name = m_Name

End Property

Public Property Let name(ByVal strName As String)

#If DEBUG_MODE > 2 Then
    Log.Protocol typeName(Me), "name", "", "strName", strName
#End If

m_Name = strName
BuildNameExt
BuildPath

End Property

Public Property Get ext() As String

ext = m_Extension

End Property

Public Property Let ext(ByVal strext As String)

#If DEBUG_MODE > 2 Then
    Log.Protocol typeName(Me), "ext", "", "strext", strext
#End If

m_Extension = strext
BuildNameExt
BuildPath

End Property

Public Property Get nameext() As String

nameext = m_NameExt

End Property

Public Property Let nameext(ByVal strNameext As String)

#If DEBUG_MODE > 2 Then
    Log.Protocol typeName(Me), "nameext", "", "strNameext", strNameext
#End If

m_NameExt = strNameext
SplitNameExt
BuildPath

End Property

Private Sub BuildNameExt()

If m_Name <> "" And m_Extension <> "" Then
    m_NameExt = m_Name & "." & m_Extension
Else
    m_NameExt = m_Name & m_Extension
End If

End Sub

Private Sub SplitNameExt()

Dim p As Integer

p = InStrRev(m_NameExt, ".", -1, vbTextCompare)
If p > 0 Then
    m_Name = Left(m_NameExt, p - 1)
    m_Extension = Mid(m_NameExt, p + 1)
Else
    m_Name = m_NameExt
    m_Extension = ""
End If

End Sub

Private Sub BuildPath()

m_Fullpath = ""
If Len(m_Drive) > 0 Then m_Fullpath = m_Drive & ":"
m_Fullpath = m_Fullpath & m_Path & m_NameExt

End Sub

Private Sub SplitPath()

Dim help As String
Dim p As Integer

reset
help = m_Fullpath

' Drive
p = InStr(1, help, ":")
If p > 0 Then
    m_Drive = Left(help, p - 1)
    If Len(m_Drive) > 1 Then m_Drive = Left(m_Drive, 1)
    help = Mid(help, p + 1)
End If

' The last part if the path is supposed to be the Name
p = InStrRev(help, "\")
m_Name = Mid(help, p + 1)
help = Left(help, p)
p = InStrRev(m_Name, ".")
If p > 0 Then
    m_Extension = Mid(m_Name, p + 1)
    m_Name = Left(m_Name, p - 1)
End If
BuildNameExt

' Path
m_Path = help

End Sub

Public Property Get FileExists() As Boolean

Const FUNCTION_NAME = "FileExists"

Dim es As tSavedError

'------------------------

On Error GoTo Catch

#If DEBUG_MODE > 2 Then
    Log.Protocol typeName(Me), FUNCTION_NAME, ""
#End If

If fso Is Nothing Then Set fso = New Scripting.FileSystemObject
FileExists = fso.FileExists(Me.fullPath)

'------------------------
Final:
On Error Resume Next
Set fso = Nothing

On Error GoTo 0
RaiseSavedError es
Exit Property

'------------------------
Catch:
es = SaveError(typeName(Me), FUNCTION_NAME, Erl)
Select Case es.number
    Case Else
        Debug.Print es.number, es.source, es.description
End Select
Resume Final
Resume ' for test purposes only

End Property
